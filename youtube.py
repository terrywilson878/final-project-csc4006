import csv
import os
import pickle
import re
import time
from datetime import datetime, timezone, timedelta

import isodate
import matplotlib.pyplot as plt
import pandas as pd
import plotly.graph_objects as go
from dateutil import parser
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from skcriteria import Data, MIN, MAX
from skcriteria.madm import simple
from sklearn.preprocessing import minmax_scale
from textblob import TextBlob
from multiprocessingyoutube import Pool

SCOPES = [
    'https://www.googleapis.com/auth/youtube.readonly',
    'https://www.googleapis.com/auth/youtube.force-ssl'
]

durations = []
likes = []
allvideoids = []
api = "AIzaSyAWfVoKHb8goDQlZ8_BnxHkiN5AxpCFMqU"


def youtube_authenticate():
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "static/credentials/credentials.json"
    credentials = None
    # the file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time
    if os.path.exists("token.pickle"):
        with open("token.pickle", "rb") as token:
            credentials = pickle.load(token)
    # checks for valid credentials and there isnt any present it asks the user to login
    if not credentials or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(client_secrets_file, SCOPES)
            flow.run_local_server(port=8080, prompt="consent", authorization_prompt_message="")
            credentials = flow.credentials
        # save the credentials for the next run
        with open("token.pickle", "wb") as token:
            pickle.dump(credentials, token)

    return build(api_service_name, api_version, credentials=credentials)


# authenticate to YouTube API
youtube = youtube_authenticate()


def getvideos(**kwargs):
    return youtube.videos().list(
        part="snippet,contentDetails,statistics",
        **kwargs
    ).execute()


def readcsv(filename):
    channels = []
    with open('static/files/' + filename) as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            channels.append(row)
    return channels


def get50VideoFromChannel(channel):
    i = str(channel).rfind("/")
    channelid = str(channel)[i + 1:]
    # The channel obtained above si then used in a request
    request = youtube.channels().list(
        part="contentDetails",
        id=channelid,
    ).execute()
    playlistid = request['items'][0]['contentDetails']['relatedPlaylists']['uploads']
    request2 = youtube.playlistItems().list(
        part="snippet",
        playlistId=playlistid,
        maxResults="20"
    ).execute()
    return request2


def getAllVideoFromChannel(channel):
    # The channel id is obatined by finding the last path of url and splitting it
    # from the rest fo the string and then removes the last 2 characters
    i = str(channel).rfind("/")
    channelid = str(channel)[i + 1:]
    # The channel obtained above si then used in a request
    request = youtube.channels().list(
        part="contentDetails",
        id=channelid,
    ).execute()
    playlistid = request['items'][0]['contentDetails']['relatedPlaylists']['uploads']
    request2 = youtube.playlistItems().list(
        part="snippet",
        playlistId=playlistid,
        maxResults="50"
    ).execute()
    npt = request2.get("nextPageToken")
    while ('nextPageToken' in request2):
        request3 = youtube.playlistItems().list(
            part="snippet",
            playlistId=playlistid,
            maxResults="50",
            pageToken=npt
        ).execute()
        request2['items'] = request2['items'] + request3['items']
        if 'nextPageToken' not in request3:
            request2.pop('nextPageToken', None)
        else:
            npt = request3['nextPageToken']
    return request2


def writecsv(details):
    fieldnames = ["title", "videoUrl", "description", "numComments", "numLikes", "numViews", "duration"]
    with open('mydata.csv', 'w', encoding="utf-8") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for k in details:
            writer.writerow({field: details[k].get(field) or k for field in fieldnames})


def getVideoDetails(response):
    videos = response["items"]
    for video in videos:
        video_id = video['id']
        title = video['snippet']['title']
        thumbnail = video['snippet']['thumbnails']['medium']['url']
        description = video['snippet']['description']
        comment_count = video['statistics']['commentCount']
        like_count = video['statistics']['likeCount']
        likes.append(like_count)
        view_count = video['statistics']['viewCount']
        duration = video['contentDetails']['duration']
        durations.append(duration)
        dur = isodate.parse_duration(duration)
        #num_positive_comments = getNumberPositiveComments(video_id)
        latest_comment = datetime.now(timezone.utc).strftime("%d/%m/%Y, %H:%M:%S")
        if int(comment_count) != 0:
            latest_comment = getlatestcomment(videoId=video_id)
            latest_comment = isodate.parse_datetime(latest_comment).strftime("%d/%m/%Y, %H:%M:%S")
        upload_date = parser.isoparse(video['snippet']['publishedAt'])
        days_from_upload = datetime.now(timezone.utc) - upload_date
        if days_from_upload.days == 0:
            days_from_upload = timedelta(days=1)
        days_from_last_comment = datetime.now(timezone.utc) - upload_date
        days = days_from_last_comment.days
        if days == 0:
            microseconds = (days_from_last_comment.seconds * 0.000001) + days_from_last_comment.microseconds
            days = microseconds / 86400000000
        details = {"title": title,
                   "videoUrl": thumbnail,
                   "description": description,
                   "numComments": comment_count,
                   "numLikes": like_count,
                   "numViews": view_count,
                   "duration": str(dur),
                   #"numPositiveComments": num_positive_comments,
                   "latestComment": latest_comment,
                   "videoid": video_id
                   }

        rank_details = [title, round(int(comment_count) / days_from_upload.days),
                        round(int(like_count) / days_from_upload.days),
                        round(int(view_count) / days_from_upload.days), round(dur.total_seconds()), days
                        #,num_positive_comments
        ]
    return details, rank_details


def getlatestcomment(**kwargs):
    results = youtube.commentThreads().list(
        part="snippet",
        **kwargs,
        textFormat="plainText"
    ).execute()
    comment = results['items'][0]
    publishedDate = comment['snippet']['topLevelComment']['snippet']['publishedAt']
    return publishedDate


def getallcomments(**kwargs):
    comments = []
    results = youtube.commentThreads().list(
        part="snippet",
        **kwargs,
        maxResults=100
    ).execute()
    npt = results.get('nextPageToken')
    while 'nextPageToken' in results:
        results2 = youtube.commentThreads().list(
            part="snippet",
            **kwargs,
            textFormat="plainText",
            pageToken=npt
        ).execute()
        results['items'] = results['items'] + results2['items']
        if 'nextPageToken' not in results2:
            results2.pop('nextPageToken', None)
        else:
            npt = results2['nextPageToken']
    for result1 in results['items']:
        comments.append(result1['snippet']['topLevelComment']['snippet']['textDisplay'])
        if result1['snippet']['totalReplyCount'] > 0:
            topCommeentId = result1['snippet']['topLevelComment']['id']
            results2 = youtube.comments().list(part='snippet', maxResults='100', parentId=topCommeentId,
                                               textFormat="plainText").execute()
            for result2 in results2["items"]:
                comments.append(result2['snippet']["textDisplay"])
    return comments


def getNumberPositiveComments(comments):
    count = 0
    for result in comments:
        text = re.sub(r'[^\w]', ' ', result)
        polarity = TextBlob(text).sentiment.polarity
        if polarity > 0:
            count += 1
    return count


def getCommentData(videoid):
    results = getallcomments(videoId=videoid)
    polarities = []
    for result in results:
        text = re.sub(r'[^\w]', ' ', result)
        polarity = TextBlob(text).sentiment.polarity
        if polarity < 0:
            polarities.append('negative')
        elif polarity == 0:
            polarities.append('neutral')
        else:
            polarities.append('positive')
    return polarities


def getCommentSentimentality(videoid):
    polaritites = getCommentData(videoid)
    positive = []
    negative = []
    neutral = []
    for p in polaritites:
        if p == 'positive':
            positive.append(p)
        elif p == 'negative':
            negative.append(p)
        else:
            neutral.append(p)
    percentages = [['Positive', len(positive)], ['Negative', len(negative)],
                   ['Neutral', len(neutral)]]
    return percentages


def convertlisttodict(details):
    requiredDict = {}
    for detail in details:
        id = details.index(detail)
        id = str(id)
        requiredDict[id] = {}
        requiredDict[id] = detail
    return requiredDict


def getlikevsdurationgraph():
    plt.plot(durations, likes, marker='o')
    plt.title('Likes Vs. Duration', fontsize=14)
    plt.xlabel('Duration', fontsize=12)
    plt.ylabel('Likes', fontsize=12)
    plt.grid(True)
    plt.show()


def normalize_data(ydata, logic="minmax"):
    df = ydata.iloc[:, 1:].values.copy()
    if logic == "minmax":
        normalized_data = minmax_scale(df)
        normalized_data[:, 2] = 1 - normalized_data[:, 2]
        normalized_data[:, 4] = 1 - normalized_data[:, 4]
    elif logic == "sumNorm":
        normalized_data = df / df.sum(axis=0)
        normalized_data[:, 2] = 1 / normalized_data[:, 2]
        normalized_data[:, 4] = 1 / normalized_data[:, 4]
    elif logic == "maxNorm":
        normalized_data = df / df.max(axis=0)
        normalized_data[:, 2] = 1 / normalized_data[:, 2]
        normalized_data[:, 4] = 1 / normalized_data[:, 4]
    return normalized_data


def getRanksForDifferentsFormula(videodetails):
    video_info = ['Title', 'Comment Count', 'Like Count', 'View Count', 'Duration', 'Latest Comment',
                  'Positive Comments']
    ranks = [4, 1, 3, 6, 5, 2]
    videodetails.insert(0, video_info)
    videodetails = pd.DataFrame(videodetails[1:], columns=videodetails[0])
    videodetails = videodetails.loc[:, video_info].head(
        len(videodetails))
    copy = videodetails.copy()
    videodetails = Data(videodetails.iloc[:, 1:], [MAX, MAX, MAX, MIN, MIN, MAX], anames=videodetails['Title'],
                        cnames=videodetails.columns[1:], weights=calculateweights(ranks))
    # weighted sum, sumNorm
    dm = simple.WeightedSum(mnorm="sum")
    dec = dm.decide(videodetails)
    copy.loc[:, 'rank_weightedSum_sumNorm_inverse'] = dec.rank_

    # weighted sum, maxNorm
    dm = simple.WeightedSum(mnorm="max")
    dec = dm.decide(videodetails)
    copy.loc[:, 'rank_weightedSum_maxNorm_inverse'] = dec.rank_

    # weighted product, sumNorm
    dm = simple.WeightedProduct(mnorm="sum")
    dec = dm.decide(videodetails)
    copy.loc[:, 'rank_weightedProduct_sumNorm_inverse'] = dec.rank_

    # weighted product, sumNorm
    dm = simple.WeightedProduct(mnorm="max")
    dec = dm.decide(videodetails)
    copy.loc[:, 'rank_weightedProduct_maxNorm_inverse'] = dec.rank_

    # min max scale + mirror
    copy.loc[:, 'rank_weightedSum_minmaxScale_subtract'] = \
        pd.Series(normalize_data(copy).sum(axis=1)).rank(ascending=False).astype(int)

    # sort for better visualization
    copy.sort_values(by=['rank_weightedSum_sumNorm_inverse'], inplace=True)

    fig = go.Figure(data=go.Parcoords(
        dimensions=list([
            dict(label='Video Title', range=[1, 10], tickvals=list(range(1, 11)), values=list(range(1, 11)),
                 ticktext=copy['Title']),
            dict(label='WeightedSum(sum)', values=copy['rank_weightedSum_sumNorm_inverse']),
            dict(label='WeightedSum(max)', values=copy['rank_weightedSum_maxNorm_inverse']),
            dict(label='WeightedProduct(sum)', values=copy['rank_weightedProduct_sumNorm_inverse']),
            dict(label='WeightedProduct(max)', values=copy['rank_weightedProduct_maxNorm_inverse']),
            dict(label='MinMax_subtract', values=copy['rank_weightedSum_minmaxScale_subtract']),
        ])
    )
    )
    fig.update_layout(plot_bgcolor='white', paper_bgcolor='white')
    fig.show()


def calculateweights(ranks):
    weights = []
    for rank in ranks:
        n_rank = (2 * (len(ranks) + 1 - rank)) / (pow(len(ranks), 2) + len(ranks))
        weights.append(n_rank)
    return weights


def getweightedproductrank(video_details):
    video_info = ['Title', 'Comment Count', 'Like Count', 'View Count', 'Duration', 'Latest Comment']
    ranks = [3, 1, 2, 5, 4]
    weights = calculateweights(ranks)
    video_copy = video_details.copy()
    video_copy.insert(0, video_info)
    video_copy = pd.DataFrame(video_copy[1:], columns=video_copy[0])
    video_copy = video_copy.loc[:, video_info].head(
        len(video_details))
    copy = video_copy.copy()
    video_copy = Data(video_copy.iloc[:, 1:], [MAX, MAX, MAX, MIN, MIN], anames=video_copy['Title'],
                      cnames=video_copy.columns[1:], weights=weights)
    dm = simple.WeightedProduct(mnorm="sum")
    dec = dm.decide(video_copy)
    copy.loc[:, 'rank'] = dec.rank_
    return copy.to_dict('records')


def sortByDuration(value):
    h, m, s = value.split(':')
    sec = int(timedelta(hours=int(h), minutes=int(m), seconds=int(s)).total_seconds())
    return sec


def chunks(list, size):
    return (list[pos:pos + size] for pos in range(0, len(list), size))

if __name__ == '__main__':
    t0 = time.time()
    channels = readcsv("youtubelink.csv")
    videodetails = []
    rankdetails = []
    csentiments=[]
    videoids=[]
    for link in channels:
        request = get50VideoFromChannel(link[0])
    for video in request['items']:
        videoid = video['snippet']['resourceId']['videoId']
        videoids.append(videoid)
        for video_id in chunks(videoids,50):
            video_id = ",".join(video_id)
            response = getvideos(id=video_id)
            details, rank = getVideoDetails(response)
            videodetails.append(details)
            rankdetails.append(rank)
    print(videodetails)
    print(rankdetails)
    t1 = time.time()
    total = t1-t0
    print(total)
   # getRanksForDifferentsFormula(rankdetails)
#     ranks = [i['rank']for i in getWeightedSumRank(rankdetails)]
#     i=0
#     for detail in videodetails:
#         detail['rank']=ranks[i]
#         i+=1
#     videodetails = sorted(videodetails, key=lambda d: d['rank'])
#     print(*videodetails, sep='\n')
#
