import csv
import os
import pickle
import re
from datetime import datetime
from itertools import islice

from dateutil import parser
import isodate
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

from MySQLdb import connect, cursors
from textblob import TextBlob

from static.keys.key4 import key

SCOPES = [
    'https://www.googleapis.com/auth/youtube.readonly',
    'https://www.googleapis.com/auth/youtube.force-ssl'
]

api_service_name = "youtube"
api_version = "v3"

config = {
    'host': 'youtubedataserver2.mysql.database.azure.com',
    'user': 'twilson20',
    'password': 'ccXxDlHlF4',
    'database': 'youtubedatadatabase',
    'cursorclass': cursors.SSCursor,
    'ssl': {'ssl' : {'ca': '../static/files/DigiCertGlobalRootG2.crt.pem'}}
}


def youtube_authenticate():
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    api_service_name = "youtube"
    api_version = "v3"
    client_secrets_file = "static/credentials/credentials.json"
    credentials = None
    # the file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first time
    if os.path.exists("token.pickle"):
        with open("token.pickle", "rb") as token:
            credentials = pickle.load(token)
    # checks for valid credentials and there isnt any present it asks the user to login
    if not credentials or not credentials.valid:
        if credentials and credentials.expired and credentials.refresh_token:
            credentials.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(client_secrets_file, SCOPES)
            flow.run_local_server(port=8080, prompt="consent", authorization_prompt_message="")
            credentials = flow.credentials
        # save the credentials for the next run
        with open("token.pickle", "wb") as token:
            pickle.dump(credentials, token)

    return build(api_service_name, api_version, credentials=credentials)


# authenticate to YouTube API
# youtube = youtube_authenticate()
youtube = build(api_service_name, api_version, developerKey=key)


def readcsv(filename):
    channels = []
    with open('static/files/' + filename) as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            channels.append(row)
    return channels


def getChannels(channelid):
    response = youtube.channels().list(
        part="snippet, contentDetails, statistics",
        id=channelid
    ).execute()
    return response


def getchanneldata(response):
    channels = response['items']
    for channel in channels:
        c_id = channel.get('id')
        title = channel.get('snippet').get('title')
        viewCount = channel.get('statistics').get('videoCount')
    return [c_id, title, viewCount]


def getvideos(**kwargs):
    return youtube.videos().list(
        part="snippet,contentDetails,statistics",
        **kwargs
    ).execute()


def get50VideoFromChannel(channelid):
    # The channel obtained above si then used in a request
    request = youtube.channels().list(
        part="contentDetails",
        id=channelid,
    ).execute()
    playlistid = request['items'][0]['contentDetails']['relatedPlaylists']['uploads']
    request2 = youtube.playlistItems().list(
        part="snippet",
        playlistId=playlistid,
        maxResults="50"
    ).execute()
    return request2


def getAllVideoFromChannel(channel):
    # The channel id is obatined by finding the last path of url and splitting it
    # from the rest fo the string and then removes the last 2 characters
    i = str(channel).rfind("/")
    channelid = str(channel)[i + 1:]
    # The channel obtained above si then used in a request
    request = youtube.channels().list(
        part="contentDetails",
        id=channelid,
    ).execute()
    playlistid = request['items'][0]['contentDetails']['relatedPlaylists']['uploads']
    request2 = youtube.playlistItems().list(
        part="snippet",
        playlistId=playlistid,
        maxResults="50"
    ).execute()
    npt = request2.get("nextPageToken")
    while ('nextPageToken' in request2):
        request3 = youtube.playlistItems().list(
            part="snippet",
            playlistId=playlistid,
            maxResults="50",
            pageToken=npt
        ).execute()
        request2['items'] = request2['items'] + request3['items']
        if 'nextPageToken' not in request3:
            request2.pop('nextPageToken', None)
        else:
            npt = request3['nextPageToken']
    return request2


def getVideoData(response, latest_comment, num_positive_comments, channel_id):
    videos = response['items']
    view_count = 0
    comment_count = 0
    like_count = 0
    for video in videos:
        vid = video.get('id')
        title = video.get('snippet').get('title')
        if video.get('statistics').get('viewCount'):
            view_count = video.get('statistics').get('viewCount')
        if video.get('statistics').get('commentCount'):
            comment_count = video.get('statistics').get('commentCount')
        if video.get('statistics').get('likeCount'):
            like_count = video.get('statistics').get('likeCount')
        description = video.get('snippet').get('description')
        url = video.get('snippet').get('thumbnails').get('default').get('url')
        duration = video.get('contentDetails').get('duration')
        upload_date = video.get('snippet').get('publishedAt')
        upload_date = parser.parse(upload_date).strftime('%d-%m-%y %H:%M:%S')
    return [vid, title, view_count, comment_count, like_count, description, url, duration, upload_date, latest_comment,
            num_positive_comments, channel_id]


def getAllComments(videoid):
    comments = []
    response = youtube.commentThreads().list(
        part='snippet,replies',
        maxResults=100,
        textFormat='plainText',
        videoId=videoid
    ).execute()
    comments.extend(getCommentDetails(response, videoid))

    while response:
        if 'nextPageToken' in response:
            response = youtube.commentThreads().list(
                part='snippet,replies',
                maxResults=100,
                textFormat='plainText',
                videoId=videoid,
                pageToken=response['nextPageToken']
            ).execute()
            comments.extend(getCommentDetails(response, videoid))
        else:
            break
    return comments


def getCommentDetails(results, videoid):
    comments = []
    if len(results) > 0:
        for result1 in results['items']:
            comment=result1['snippet']['topLevelComment']['snippet']['textDisplay']
            if 'pi' in comment or '3.141' in comment:
                comment = comment.split("0", 1)[0]
            topcommentdetail = (videoid, result1['snippet']['topLevelComment']['id'],
                                comment,
                                result1['snippet']['topLevelComment']['snippet']['likeCount'],
                                parser.parse(result1['snippet']['topLevelComment']['snippet']['publishedAt']).strftime(
                                    '%d-%m-%y %H:%M:%S'), TextBlob(
                    re.sub(r'[^\w]', ' ', result1['snippet']['topLevelComment']['snippet']['textDisplay'])
                ).sentiment.polarity)
            comments.append(topcommentdetail)
    return comments


def getpositivecomments(comment):
    text = re.sub(r'[^\w]', ' ', comment)
    polarity = TextBlob(text).sentiment.polarity
    if polarity > 0:
        return 1
    else:
        return 0


def getallpostivecomments(comments):
    return sum([1 if result[5] > 0 else 0 for result in comments])


def getlatestcomment(comments):
    value = max(comments, key=lambda x: x[4])[4]
    return value


if __name__ == '__main__':
    channels = readcsv("youtubelink.csv")
    channel_ids = []
    video_ids = []
    comments = []
    try:
        connection = connect(**config)
        print("Connection Established")
    except Exception as e:
        print(e)
    cursor = connection.cursor()
    for link in channels:
        i = str(link[0]).rfind("/")
        channelid = str(link[0])[i + 1:]
        channel_ids.append(channelid)
    # cursor.execute(query="SELECT channel_id FROM channeldata WHERE channel_id NOT IN (SELECT channel_id FROM videodata)")
    # channel_ids = cursor.fetchall()
    for channel_id in channel_ids[11:12]:
        print(str(channel_ids.index(channel_id))+':'+channel_id)
        channel = getChannels(channel_id)
        channeldata = getchanneldata(channel)
        insert_channel = """ INSERT INTO `channeldata` (`channel_id`, `channel_name`, `num_videos`)
                             VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE 
                            `num_videos`=values(num_videos) """
        cursor.execute(insert_channel, channeldata)
        connection.commit()
        # cursor.execute(query="SELECT video_id FROM videodata WHERE video_id NOT IN (SELECT videoid FROM commentdata)")
        request = getAllVideoFromChannel(channel_id)
        for item in request['items']:
            video_id = item['snippet']['resourceId']['videoId']
            video_ids.append(video_id)
        # cursor.execute(query="SELECT DISTINCT video_id FROM videodata WHERE video_id NOT IN (SELECT video_id FROM `ucx6b17pvsybq0ip5gyeme-qcomments`)")
        # video_ids = cursor.fetchall()
        values = []
        tablename = channel_id + "comments"
        insert_video = """INSERT INTO `videodata` (`video_id`, `title`, `num_views`, `num_comments`, `num_likes`,
                                                        `description`, `image_url`, `duration`, `upload_date`, `latest_comment`, `num_positive_comments`, `channel_id`)
                                                         values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON DUPLICATE KEY UPDATE
                                                        `num_views`=values(num_views),`num_comments`=values(num_comments),`num_likes`=values(num_likes),
                                                        `upload_date`=values(upload_date), `latest_comment`=values(latest_comment), `num_positive_comments`=values(num_positive_comments), `channel_id`=values(channel_id)"""

        for id in video_ids[5222:]:
            response = getvideos(id=id)
            num_positive_comments = 0
            latest_comment = None
            comments = []
            if response['items'][0].get('statistics').get('commentCount') is not None:
                if int(response['items'][0].get('statistics').get('commentCount')) > 0:
                    comments.extend(getAllComments(id))
                    #comment = comments[857][2]
                    #comments[857] = (comments[857][0],comments[857][1],comment[:5000],comments[857][3],comments[857][4],comments[857][5])
                    # comment = comments[15954][2]
                    # comments[15954] = (comments[15954][0],comments[15954][1],comment[:5000],comments[15954][3],comments[15954][4],comments[15954][5])
                    # comment = comments[15955][2]
                    # comments[15955] = (comments[15955][0],comments[15955][1],comment[:5000],comments[15955][3],comments[15955][4],comments[15955][5])
                    if len(comments) > 0:
                        num_positive_comments = getallpostivecomments(comments)
                        latest_comment = getlatestcomment(comments)
            val1 = getVideoData(response, latest_comment, num_positive_comments, channel_id)
            cursor.execute(insert_video, val1)
            connection.commit()
            # if len(comments)>0:
            #     insert_comment = """ INSERT INTO `""" + tablename + """` (`video_id`, `comment_id`, `comment`, `likes`,
            #                 `upload_date`, `polarity`) values """ + ",".join("(%s, %s, %s, %s, %s, %s)" for _ in comments) + """ ON DUPLICATE KEY UPDATE `likes`=values(likes), `polarity`=values(polarity)"""
            #     flattened_comments_values = [item for sublist in comments for item in sublist]
            #     cursor.execute(insert_comment, flattened_comments_values)
            # connection.commit()
            print(str(video_ids.index(id))+'.'+id + ': Comments done')
            # print('video done')
        print('channel done')
