import asyncio
import csv
import os
import pickle
import time
from datetime import datetime, timezone, timedelta

import aiohttp as aiohttp
import isodate
import requests
from dateutil import parser
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from multiprocessing import Pool
import youtube as y
from static.keys.key6 import key

SCOPES = [
    'https://www.googleapis.com/auth/youtube.readonly',
    'https://www.googleapis.com/auth/youtube.force-ssl'
]

durations = []
likes = []
allvideoids = []

def get50VideoFromChannel(channel):
    i = str(channel).rfind("/")
    channelid = str(channel)[i + 1:]
    # The channel obtained above si then used in a request
    url = f'https://www.googleapis.com/youtube/v3/channels?id={channelid}&part=contentDetails&key={key}'
    r = requests.get(url)
    results = r.json()['items']
    playlist_id = results[0]['contentDetails']['relatedPlaylists']['uploads']
    url = f'https://www.googleapis.com/youtube/v3/playlistItems?playlistId={playlist_id}&part=contentDetails&maxResults=20&key={key}'
    r = requests.get(url)
    results = r.json()
    if 'items' in results:
        for item in results['items']:
            videoId = item['contentDetails']['videoId']
            allvideoids.append(videoId)

def getAllVideoFromChannel(channel):
    i = str(channel).rfind("/")
    channelid = str(channel)[i + 1:]
    # The channel obtained above si then used in a request
    url = f'https://www.googleapis.com/youtube/v3/channels?id={channelid}&part=contentDetails&key={key}'
    r = requests.get(url)
    results = r.json()['items']
    playlist_id = results[0]['contentDetails']['relatedPlaylists']['uploads']
    url = f'https://www.googleapis.com/youtube/v3/playlistItems?playlistId={playlist_id}&part=contentDetails&maxResults=50&key={key}'
    while True:
        r = requests.get(url)
        results = r.json()
        if 'nextPageToken' in results:
            nextPageToken = results['nextPageToken']
        else:
            nextPageToken = None
        if 'items' in results:
            for item in results['items']:
                videoId = item['contentDetails']['videoId']
                allvideoids.append(videoId)
        if nextPageToken:
            url = f'https://www.googleapis.com/youtube/v3/playlistItems?playlistId={playlist_id}&pageToken={nextPageToken}&part=contentDetails&maxResults=50&key={key}'
        else:
            break

def readcsv(filename):
    channels = []
    with open('static/files/' + filename) as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            channels.append(row)
    return channels

def viddetails(result):
    video_id = result['id']
    title = result['snippet']['title']
    thumbnail = result['snippet']['thumbnails']['medium']['url']
    description = result['snippet']['description']
    comment_count = result['statistics']['commentCount']
    like_count = result['statistics']['likeCount']
    likes.append(like_count)
    view_count = result['statistics']['viewCount']
    duration = result['contentDetails']['duration']
    durations.append(duration)
    dur = isodate.parse_duration(duration)
    num_positive_comments = y.getNumberPositiveComments(video_id)
    latest_comment = datetime.now(timezone.utc).strftime("%d/%m/%Y, %H:%M:%S")
    if int(comment_count) != 0:
        latest_comment = y.getlatestcomment(videoId=video_id)
        latest_comment = isodate.parse_datetime(latest_comment).strftime("%d/%m/%Y, %H:%M:%S")
    upload_date = parser.isoparse(result['snippet']['publishedAt'])
    days_from_upload = datetime.now(timezone.utc) - upload_date
    if days_from_upload.days == 0:
        days_from_upload = timedelta(days=1)
    days_from_last_comment = datetime.now(timezone.utc) - upload_date
    days = days_from_last_comment.days
    if days == 0:
        microseconds = (days_from_last_comment.seconds * 0.000001) + days_from_last_comment.microseconds
        days = microseconds / 86400000000
    details = {"title": title,
               "videoUrl": thumbnail,
               "description": description,
               "numComments": comment_count,
               "numLikes": like_count,
               "numViews": view_count,
               "duration": str(dur),
               "numPositiveComments": num_positive_comments,
               "latestComment": latest_comment,
               "videoid": video_id
               }

    rank_details = [title, round(int(comment_count) / days_from_upload.days),
                    round(int(like_count) / days_from_upload.days),
                    round(int(view_count) / days_from_upload.days), round(dur.total_seconds()), days,
                    num_positive_comments]
    return details, rank_details

async def get_video_data(session, video_id):
    video_id = ",".join(video_id)
    url = f'https://www.googleapis.com/youtube/v3/videos?id={video_id}&part=statistics,snippet,contentDetails&key={key}'
    videodetails=[]
    async with session.get(url) as response:
        result_data = await response.json()
        p = Pool(len(result_data))
        for details, rank in p.map(viddetails, result_data['items']):
            videodetails.append(details)
            videodetails.append(rank)
            p.close()
            p.join()
    return videodetails


def chunks(list, size):
    return (list[pos:pos + size] for pos in range(0, len(list), size))


async def main():
    async with aiohttp.ClientSession() as session:
        tasks = []
        for video_id in chunks(allvideoids,5):
            task = asyncio.ensure_future(get_video_data(session, video_id))
            tasks.append(task)
        details = await asyncio.gather(*tasks)
    print(details)


if __name__ == '__main__':
    t0 = time.time()
    channels = readcsv("youtubelink.csv")
    videodetails = []
    rankdetails = []

    for link in channels:
        request = get50VideoFromChannel(link[0])

    asyncio.run(main())

    t1 = time.time()
    total = t1-t0
    print(total)
