import csv
import re

import numpy
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from textblob import TextBlob

from static.keys.key2 import key
from dateutil import parser

from MySQLdb import connect, cursors

config = {
    'host': 'youtubedataserver2.mysql.database.azure.com',
    'user': 'twilson20',
    'password': 'ccXxDlHlF4',
    'database': 'youtubedatadatabase',
    'cursorclass': cursors.SSCursor,
    'ssl': {'ssl' : {'ca': '../static/files/DigiCertGlobalRootG2.crt.pem'}}
}

api_service_name = "youtube"
api_version = "v3"
youtube = build(api_service_name, api_version, developerKey=key)


def readcsv(filename):
    channels = []
    with open('static/files/' + filename) as f:
        reader = csv.reader(f)
        next(reader, None)
        for row in reader:
            channels.append(row)
    return channels


def getchannels(channelid):
    response = youtube.channels().list(
        part="snippet, contentDetails, statistics",
        id=channelid
    ).execute()
    return response


def getvideos(**kwargs):
    return youtube.videos().list(
        part="snippet,contentDetails,statistics",
        **kwargs
    ).execute()


def getAllVideoFromChannel(channel):
    # The channel id is obatined by finding the last path of url and splitting it
    # from the rest fo the string and then removes the last 2 characters
    i = str(channel).rfind("/")
    channelid = str(channel)[i + 1:]
    # The channel obtained above si then used in a request
    request = youtube.channels().list(
        part="contentDetails",
        id=channelid,
    ).execute()
    playlistid = request['items'][0]['contentDetails']['relatedPlaylists']['uploads']
    request2 = youtube.playlistItems().list(
        part="snippet",
        playlistId=playlistid,
        maxResults="50"
    ).execute()
    npt = request2.get("nextPageToken")
    while ('nextPageToken' in request2):
        request3 = youtube.playlistItems().list(
            part="snippet",
            playlistId=playlistid,
            maxResults="50",
            pageToken=npt
        ).execute()
        request2['items'] = request2['items'] + request3['items']
        if 'nextPageToken' not in request3:
            request2.pop('nextPageToken', None)
        else:
            npt = request3['nextPageToken']
    return request2


def getAllComments(videoid):
    comments = []
    response = youtube.commentThreads().list(
        part='snippet,replies',
        maxResults=100,
        textFormat='plainText',
        videoId=videoid
    ).execute()
    comments.extend(getCommentDetails(response, videoid))

    while response:
        if 'nextPageToken' in response:
            response = youtube.commentThreads().list(
                part='snippet,replies',
                maxResults=100,
                textFormat='plainText',
                videoId=videoid,
                pageToken=response['nextPageToken']
            ).execute()
            comments.extend(getCommentDetails(response, videoid))
        else:
            break
    return comments


def getCommentDetails(results, videoid):
    comments = []
    if len(results) > 0:
        for result1 in results['items']:
            comment=result1['snippet']['topLevelComment']['snippet']['textDisplay']
            if 'pi' in comment or '3.141' in comment:
                comment = comment.split("0", 1)[0]
            topcommentdetail = (videoid, result1['snippet']['topLevelComment']['id'],
                                comment,
                                result1['snippet']['topLevelComment']['snippet']['likeCount'],
                                parser.parse(result1['snippet']['topLevelComment']['snippet']['publishedAt']).strftime(
                                    '%d-%m-%y %H:%M:%S'), TextBlob(
                re.sub(r'[^\w]', ' ', result1['snippet']['topLevelComment']['snippet']['textDisplay'])
            ).sentiment.polarity)
            comments.append(topcommentdetail)
    return comments


def getpositivecomments(comment):
    text = re.sub(r'[^\w]', ' ', comment)
    polarity = TextBlob(text).sentiment.polarity
    if polarity > 0:
        return 1
    else:
        return 0


def getallpostivecomments(comments):
    return sum([1 if result[5] > 0 else 0 for result in comments])


def getlatestcomment(comments):
    value = max(comments, key=lambda x: x[4])[4]
    return value

def getVideoData(response):
    comments=[]
    videodetails=[]
    videos = response['items']
    view_count = 0
    comment_count = 0
    like_count = 0
    num_positive_comments = 0
    latest_comment = None
    for video in videos:
        vid = video.get('id')
        channelid = video.get('snippet').get('channelId')
        title = video.get('snippet').get('title')
        if video.get('statistics').get('viewCount'):
            view_count = video.get('statistics').get('viewCount')
        if video.get('statistics').get('commentCount'):
            comment_count = video.get('statistics').get('commentCount')
        if video.get('statistics').get('likeCount'):
            like_count = video.get('statistics').get('likeCount')
        description = video.get('snippet').get('description')
        url = video.get('snippet').get('thumbnails').get('default').get('url')
        duration = video.get('contentDetails').get('duration')
        upload_date = video.get('snippet').get('publishedAt')
        upload_date = parser.parse(upload_date).strftime('%d-%m-%y %H:%M:%S')
        if video.get('statistics').get('commentCount') is not None:
            if int(video.get('statistics').get('commentCount')) > 0:
                comments.extend(getAllComments(vid))
                num_positive_comments = getallpostivecomments(comments)
                latest_comment = getlatestcomment(comments)
        videodetails.append([vid, title, view_count, comment_count, like_count, description, url, duration, upload_date, latest_comment,
                             num_positive_comments, channelid])
    return videodetails, comments


if __name__ == '__main__':
    channels = readcsv("youtubelink.csv")
    channel_ids = []
    video_ids = []
    try:
        connection = connect(**config)
        print("Connection Established")
    except Exception as e:
        print(e)
    cursor = connection.cursor()
    insert_video = """INSERT INTO `videodata` (`video_id`, `title`, `num_views`, `num_comments`, `num_likes`,
                                                        `description`, `image_url`, `duration`, `upload_date`, `latest_comment`, `num_positive_comments`, `channel_id`)
                                                         values """ + ",".join("(%s, %s, %s, %s, %s, %s)" for i in range(50)) + """ ON DUPLICATE KEY UPDATE
                                                        `num_views`=values(num_views),`num_comments`=values(num_comments),`num_likes`=values(num_likes),
                                                        `upload_date`=values(upload_date), `latest_comment`=values(latest_comment), `num_positive_comments`=values(num_positive_comments), `channel_id`=values(channel_id)"""

    for link in channels:
        i = str(link[0]).rfind("/")
        channelid = str(link[0])[i + 1:]
        channel_ids.append(channelid)

    for channel_id in channel_ids[2:3]:
        print(str(channel_ids.index(channel_id))+':'+channel_id)
        channel = getchannels(channel_id)
        request = getAllVideoFromChannel(channel_id)
        for item in request['items']:
            video_id = item['snippet']['resourceId']['videoId']
            video_ids.append(video_id)
        chunks = [video_ids[x:x + 25] for x in range(0, len(video_ids), 25)]
        for chunk in chunks[2:]:
            insert_video = """INSERT INTO `videodata` (`video_id`, `title`, `num_views`, `num_comments`, `num_likes`,
                                                        `description`, `image_url`, `duration`, `upload_date`, `latest_comment`, `num_positive_comments`, `channel_id`)
                                                         values """ + ",".join("(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" for _ in chunk) + """ ON DUPLICATE KEY UPDATE
                                                        `num_views`=values(num_views),`num_comments`=values(num_comments),`num_likes`=values(num_likes),
                                                        `upload_date`=values(upload_date), `latest_comment`=values(latest_comment), `num_positive_comments`=values(num_positive_comments), `channel_id`=values(channel_id)"""

            video_id = ','.join(chunk)
            response = getvideos(id=video_id)
            video_details = getVideoData(response)[0]
            flattened_video_values = [item for sublist in video_details for item in sublist]
            cursor.execute(insert_video, flattened_video_values)
            connection.commit()
            print('Done: ' + video_id)
