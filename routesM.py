import os
import json
import datetime
from collections import OrderedDict
from io import TextIOWrapper
import multiprocessing as mp

from werkzeug.utils import secure_filename

import youtube

from flask import Flask, render_template, request, redirect, url_for, session, jsonify, flash

app = Flask(__name__)
app.secret_key = "super secret key"

messages = [{'title': 'Message One',
             'content': 'Message One Content'},
            {'title': 'Message Two',
             'content': 'Message Two Content'}
            ]

UPLOAD_FOLDER = 'static/files'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
all_videos = {}


@app.route('/')
def sayHello():
    return render_template("index.html")


@app.route('/home')
def home():
    return render_template("Home.html")

@app.route('/about')
def about():
    return render_template("About.html")


@app.route('/start', methods=('GET', 'POST'))
def start():
    all_video_details = []
    ranks_details = []
    video_ids =[]
    if request.method == 'POST':
        file = request.files['file']
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        channels = youtube.readcsv(filename)

        for link in channels:
            try:
              req = youtube.get50VideoFromChannel(link[0])
              for video in req['items']:
                  video_id = video['snippet']['resourceId']['videoId']
                  video_ids.append(video_id)
              for video_id in youtube.chunks(video_ids,int(len(video_ids)/2)):
                  video_id = ",".join(video_id)
                  response = youtube.getvideos(id=video_id)
                  p = mp.Pool(10)
                  map_result = p.map_async(youtube.getVideoDetails, response['items'])
                  results = map_result.get()
                  for i in results:
                      all_video_details.append(i[0])
                      ranks_details.append(i[1])
                  p.close()
              ranks = [i['rank'] for i in youtube.getweightedproductrank(ranks_details)]
              i = 0
              for detail in all_video_details:
                  detail['rank'] = ranks[i]
                  i += 1
              videos = youtube.convertlisttodict(all_video_details)
              videos = OrderedDict(sorted(videos.items(), key=lambda i: i[1]['rank']))
              global all_videos
              all_videos = videos
              if len(videos) > 1:
                  return redirect(url_for("multiplevideo", data=json.dumps(all_videos)))
              else:
                  return redirect(url_for("singlevideo"))
            except Exception as e:
                flash("Please Make sure the Channels Links are correct and the file format fit the format described below.")
                redirect(url_for("start"))
    return render_template("Start.html")


@app.route('/exporttocsv')
def exporttocsv():
    youtube.writecsv(all_videos)
    return "Done."


@app.route('/singlevideo', methods=["GET", "POST"])
def singlevideo():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        video_id = request.args.get("video_id")
        response = youtube.getvideos(id=video_id)
        vid_detail,rank = youtube.getVideoDetails(response, video_id=video_id)
        return render_template('SingleVideo.html', detail=vid_detail)



@app.route('/multiplevideo', methods=["GET","POST"])
def multiplevideo():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    return render_template('MultipleVideos.html', videos=all_videos)


@app.route('/likesvsdurationgraph')
def likesvsdurationgraph():
    values = []
    legend = "Likes vs. Duration"
    labels = ["Duration","Likes"]
    array_value =""
    for k, v in all_videos.items():
        value = []
        for k, v in v.items():
            if k == 'duration':
                sec = youtube.sortByDuration(v)
                value.insert(0, sec)
            elif k == 'numLikes':
                value.insert(1, int(v))
                array_value = k
        values.append(value)
        values.sort(key = lambda x: x[0])
    values.insert(0, labels)
    return render_template('Graph.html', values=values, legend=legend, xAxis="Likes", yAxis="Duration(Seconds)", videos=all_videos, x=array_value)


@app.route('/numcommentsvsdurationgraph')
def numcommentsvsdurationgraph():
    values = []
    legend = "Comments vs. Duration"
    labels = ["Duration","Comments"]
    array_value =""
    for k, v in all_videos.items():
        value = []
        for k, v in v.items():
            if k == 'duration':
                sec = youtube.sortByDuration(v)
                value.insert(0, sec)
            elif k == 'numComments':
                value.insert(1, int(v))
                array_value = k
        values.append(value)
        values.sort(key = lambda x: x[0])
    values.insert(0, labels)
    return render_template('Graph.html', values=values, legend=legend, xAxis="Comments", yAxis="Duration(Seconds)", videos=all_videos, x=array_value)

@app.route('/numpositivecommentsforeachvideograph')
def numpositivecommentsforeachvideograph():
    values = []
    legend = "Comments vs. Duration"
    labels = ["Title","Number of Positive Comments"]
    array_value =""
    for k, v in all_videos.items():
        value = []
        for k, v in v.items():
            if k == 'title':
                value.insert(0, v)
            elif k == 'numPositiveComments':
                value.insert(1, int(v))
                array_value = k
        values.append(value)
        values.sort(key = lambda x: x[0])
    values.insert(0, labels)
    return render_template('ScatterGraph.html', values=values, legend=legend, xAxis="Number of Positive", yAxis="Title", videos=all_videos, x=array_value)


@app.route('/percentageover10000likesgraph')
def percentageover10000likesgraph():
    onethousand = 0
    tenthousand = 0
    onehundredthousand = 0
    onemillion = 0
    for k, v in all_videos.items():
        for k, v in v.items():
            if (k == 'numLikes'):
                if (int(v) <= 1000):
                    onethousand += 1
                elif (int(v) <= 10000):
                    tenthousand += 1
                elif (int(v) <= 100000):
                    onehundredthousand += 1
                elif (int(v) <= 1000000):
                    onemillion += 1
    labels=["Like Count Group", "Number of Videos"]
    values = [["<=1000 Likes", onethousand], ["<=10000", tenthousand],["<=100000 Likes", onehundredthousand],["<=1000000", onemillion]]
    values.insert(0, labels)
    return render_template('PieChart.html', values=values, videos=all_videos)

@app.route('/barchart')
def barchart():
    videoid = request.args.get("videoid")
    legend = "Sentiment(Polarity) By Number of Comments"
    labels = ["Sentiment","Number of Comments"]
    values = youtube.getCommentSentimentality(videoid)
    values.insert(0, labels)
    return render_template('ScatterGraph.html', values=values, legend=legend, xAxis="Sentiment", yAxis="Number of Comments", videoid=videoid)


if __name__ == "__main__":
    app.run(debug=True)
