import os
import json
import datetime
from collections import OrderedDict
from io import TextIOWrapper

from Scripts import chkcsv
from flask_mail import Mail, Message
from MySQLdb import connect, cursors, Error
from werkzeug.utils import secure_filename

import youtube

from flask import Flask, render_template, request, redirect, url_for, session, jsonify, flash, get_flashed_messages
from flask_debugtoolbar import DebugToolbarExtension


import youtubedata

app = Flask(__name__)
app.secret_key = "super secret key"
app.config['DEBUG_TB_PROFILER_ENABLED'] = True
app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
app.debug = True

app.config['JSON_SORT_KEYS'] = False

UPLOAD_FOLDER = 'static/files'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'videolytics438@gmail.com'
app.config['MAIL_PASSWORD'] = 'ccXxDlHlF4'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

all_videos = {}
channel_videos = {}


@app.route('/')
def sayHello():
    return render_template("index.html")


@app.route('/home')
def home():
    return render_template("Home.html")


@app.route('/about')
def about():
    return render_template("About.html")


@app.route('/contact', methods=('GET', 'POST'))
def contact():
    if request.method == 'POST':
        try:
            form = request.form
            msg = Message(form['text'], sender='videolytics438@gmail.com', recipients=['videolytics438@gmail.com'])
            msg.body = form['message'] + "\n Name: " + form['name'] + "\n Email: " + form['email']
            mail.send(msg)
            flash("Success. Message Sent.")

            return redirect(url_for("contact"))
        except Error as e:
            print(e)
            flash("Fail. Message could not be sent please re-enter details and try again.")

    return render_template("Contact.html", length=len(all_videos))


@app.route('/start', methods=('GET', 'POST'))
def start():
    channelids = []
    if request.method == 'POST':
        try:
            file = request.files['file']
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            csvchecker = chkcsv.read_format_specs('youtubelink.fmt', True, True,)
            check = chkcsv.check_csv_file("static/files/youtubelink.csv", csvchecker, False, True, False, False)
            if len(check) == 0:
                channelids=[]
                channels = youtubedata.readcsv(filename)
                for channel in channels:
                    i = str(channel).rfind("/")
                    channelid = str(channel)[i + 1:]
                    channelids.append(channelid)
                try:
                    channelsdict={}
                    channelslist = youtubedata.getallChannels()
                    for channel in channelslist:
                        channelsdict[channel[0]] = channel[1]
                    details, rank = youtubedata.getallvideodatabychannel(channelids)
                    allvideos = []
                    allranks = []
                    for i in details:
                        allvideos.append(i[1])
                    for k in rank:
                        allranks.append(k[1])
                    allvideoranks = youtubedata.getvideoweightedproductrank(allranks, allvideos)
                    allvlist = youtubedata.groupbychannelid(details)
                    allvlist['AllVideos'] = allvideos
                    videos = youtubedata.groupbychannelid(details)
                    ranks = youtubedata.groupbychannelid(rank)
                    channelranks=[]
                    for i in videos:
                        channelranks.append(youtubedata.getvideoweightedproductrank(ranks[i], videos[i]))
                    channelranks.append(allvideoranks)
                    r = 0
                    for i in allvlist:
                        x = 0
                        for vlist in allvlist[i]:
                            rank = ('rank',channelranks[r][x]['rank'])
                            vlist.append(rank)
                            x += 1
                        r += 1
                    allvlist = youtubedata.convertlisttodict(allvlist)
                    global all_videos
                    all_videos = allvlist
                    return redirect(url_for("multiplevideo", channels=json.dumps(channelsdict)))
                except Error as e:
                    flash("Error with connection to database. Please Contact the Administrator")
            else:
                errors = []
                for error in check:
                    string = "\nError: " + error[0] + " in Row: " + str(error[2])
                    errors.append(string)
                flash('There is an error in the format of the CSV file "' + check[0][1][check[0][1].rindex('/')+1:] +
                      '": <br>' + '<br>'.join(errors) + '<br> Please check the format.')

        except FileNotFoundError as e:
            flash("Please select and input a file.")

    return render_template("Start.html")


@app.route('/exporttocsv')
def exporttocsv():
    youtube.writecsv(all_videos)
    return "Done."


@app.route('/singlevideo', methods=["GET", "POST"])
def singlevideo():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        video_id = request.args.get("videoid")
        channel_id = request.args.get("channelid")
        vid_detail = None
        comments = []
        for k, v in all_videos.items():
            if k == channel_id:
                for id in v:
                    if video_id == id['videoid']:
                        vid_detail = id
                        if id['numComments'] > 0:
                            details, rank = youtubedata.getallcommentdata(video_id, channel_id)
                            ranks = [i['rank'] for i in youtubedata.getcommentweightedproductrank(rank)]
                            i = 0
                            for detail in details:
                                detail['rank'] = ranks[i]
                                i += 1
                            comments = sorted(details, key=lambda x: x['rank'])
        return render_template('SingleVideo.html', detail=vid_detail, comments=comments)


@app.route('/multiplevideo', methods=["GET", "POST"])
def multiplevideo():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        channels = request.args['channels']
        channels = json.loads(channels)
    return render_template('MultipleVideos.html', videos=all_videos, channels=channels)


@app.route('/likesvsdurationgraph', methods=["GET", "POST"])
def likesvsdurationgraph():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        videovalues = []
        graphvalues = []
        legend = "Likes vs. Duration"
        labels = ["Duration", "Likes"]
        array_value = ""
        channel_id = request.args.get("channelid")
        for key, value in all_videos.items():
            if key == channel_id:
                for val in value:
                    graphvalue = []
                    for k, v in val.items():
                        if k == 'duration':
                            sec = youtube.sortByDuration(v)
                            graphvalue.insert(0, sec)
                        elif k == 'numLikes':
                            graphvalue.insert(1, int(v))
                            array_value = k
                    graphvalues.append(graphvalue)
                    graphvalues.sort(key=lambda x: x[0])
                graphvalues.insert(0, labels)
                videovalues = value
    return render_template('Graph.html', values=graphvalues, legend=legend, xAxis="Likes",
                           yAxis="Duration(Seconds)", videos=videovalues, x=array_value)


@app.route('/numcommentsvsdurationgraph', methods=["GET", "POST"])
def numcommentsvsdurationgraph():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        videovalues = []
        graphvalues = []
        legend = "No. Comments vs. Duration"
        labels = ["Duration", "Comments"]
        array_value = ""
        channel_id = request.args.get("channelid")
        for key, value in all_videos.items():
            if key == channel_id:
                for val in value:
                    graphvalue = []
                    for k, v in val.items():
                        if k == 'duration':
                            sec = youtube.sortByDuration(v)
                            graphvalue.insert(0, sec)
                        elif k == 'numComments':
                            graphvalue.insert(1, int(v))
                            array_value = k
                    graphvalues.append(graphvalue)
                graphvalues.sort(key=lambda x: x[0])
                graphvalues.insert(0, labels)
                videovalues = value
    return render_template('Graph.html', values=graphvalues, legend=legend, xAxis="Comments", yAxis="Duration(Seconds)",
                           videos=videovalues, x=array_value)


@app.route('/numpositivecommentsforeachvideograph', methods=["GET", "POST"])
def numpositivecommentsforeachvideograph():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        videovalues = []
        graphvalues = []
        legend = "Likes vs. Duration"
        labels = ["Title", "Number of Positive Comments"]
        array_value = ""
        channel_id = request.args.get("channelid")
        for key, value in all_videos.items():
            if key == channel_id:
                for val in value:
                    graphvalue = []
                    for k, v in val.items():
                        if k == 'title':
                            index = value.index(val)
                            graphvalue.insert(0, index)
                        elif k == 'numPositiveComments':
                            graphvalue.insert(1, int(v))
                            array_value = k
                    graphvalues.append(graphvalue)
                graphvalues.insert(0, labels)
                videovalues = value
    return render_template('ScatterGraph.html', values=graphvalues, legend=legend, xAxis="Number of Positive",
                           yAxis="Title", videos=videovalues, x=array_value)


@app.route('/percentageover10000likesgraph', methods=["GET", "POST"])
def percentageover10000likesgraph():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        videovalues = []
        onethousand = 0
        tenthousand = 0
        onehundredthousand = 0
        onemillion = 0
        channel_id = request.args.get("channelid")
        for key, value in all_videos.items():
            if key == channel_id:
                for val in value:
                    for k, v in val.items():
                        if (k == 'numLikes'):
                            if (int(v) <= 1000):
                                onethousand += 1
                            elif (int(v) <= 10000):
                                tenthousand += 1
                            elif (int(v) <= 100000):
                                onehundredthousand += 1
                            elif (int(v) <= 1000000):
                                onemillion += 1
                videovalues = value
        labels = ["Like Count Group", "Number of Videos"]
        values = [["<=1000 Likes", onethousand], ["<=10000", tenthousand], ["<=100000 Likes", onehundredthousand],
                  ["<=1000000", onemillion]]
        values.insert(0, labels)
    return render_template('PieChart.html', values=values, videos=videovalues)


@app.route('/numcommentsbysentiment', methods=["GET", "POST"])
def numcomments():
    if request.method == 'POST':
        return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
    else:
        videos = []
        video_id = request.args.get("videoid")
        channel_id = request.args.get("channelid")
        legend = "Sentiment(Polarity) By Number of Comments"
        labels = ["Sentiment", "Number of Comments"]
        values = youtube.getCommentSentimentality(video_id)
        values.insert(0, labels)
        for key, value in all_videos.items():
            if key == channel_id:
                videos = value
    return render_template('BarChart.html', values=values, legend=legend, xAxis="Sentiment", yAxis="Number of Comments",
                           videoid=video_id, videos=videos, x="polarity")


if __name__ == '__main__':
    toolbar = DebugToolbarExtension(app)
    from werkzeug.middleware.profiler import ProfilerMiddleware
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[10])
    app.run(host="localhost", port=8008, debug=True)
